# Softwaretechnik - modulbezogene Übung Teil 1

Entwickeln einer API im Spring-Framework und anschließendes Ausführen von Tests (Coverage, Unit, Integration und Mutationstest).

## Implementierte Features in Spring

- Erstellen von Filmen
- Auslesen von Filmen sowie der dazugehörigen ID

Datensätze sind wie gewollt nicht persistent und werden bei Neustart der Anwendung verworfen!

## API Endpunkte
#### Get all movies

```http
  GET /movies/
```

Returns list of all movies.

<br />

#### Add movies

```http
  POST /movies/
```

| Parameter   | Type     | Description                 |
|:------------|:---------|:----------------------------|
| `movieName` | `String` | **Required**. Name of movie |


Returns status code 201 CREATED and adds the given movie to the movieList.

## Authors

- [@Niklas9401](https://github.com/Niklas9401)