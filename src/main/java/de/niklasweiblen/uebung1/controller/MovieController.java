package de.niklasweiblen.uebung1.controller;

import de.niklasweiblen.uebung1.entity.Movie;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Controller

@RequiredArgsConstructor
@Slf4j
@RequestMapping("/movies")
public class MovieController {

    private final AtomicLong idCounter = new AtomicLong();
    private final List<Movie> movieList = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        log.info("Fetching all movies from the movielist");
        return ResponseEntity.ok().body(movieList);
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movieToBeAdded) {
        if (movieToBeAdded.getName() == null || movieToBeAdded.getName().isBlank()) {
            log.error("Could not add the movie to the movielist, as the name could not be empty/blank");
            return ResponseEntity.badRequest().build();
        }
        log.info("Adding movie {} to the movielist", movieToBeAdded.getName());
        movieToBeAdded.setId(idCounter.getAndIncrement());
        movieList.add(movieToBeAdded);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
