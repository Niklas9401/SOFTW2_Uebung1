package de.niklasweiblen.uebung1.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Movie {

    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    @NonNull
    private String name;

}
