package de.niklasweiblen.uebung1.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.niklasweiblen.uebung1.entity.Movie;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)    //Sorgt für das Leeren der Movielist und dem ID-Counter
class MovieControllerIntegrationTests {

    @Autowired
    MovieController movieController;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    //Testfall 1
    @Test
    void successfulRequestWithEmptyListAsResponse() throws Exception {
        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    //Testfall 2
    @Test
    void creatingExampleMovie() throws Exception {
        Movie movie = new Movie("Top-Gun");


        mockMvc.perform(MockMvcRequestBuilders
                        .post("/movies")
                        .content(objectMapper.writeValueAsString(movie))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").doesNotExist());
    }


    //Testfall 3
    @Test
    void addMovieAndCheckExistence() throws Exception {
        String movieName = "Who am I";
        Movie movie = new Movie(movieName);


        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));


        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName))
                .andExpect(jsonPath("$.*", hasSize(1)));
    }

    //Testfall 4
    @Test
    void addTwoMoviesandCheckExistence() throws Exception {
        String movieName1 = "King Kong";
        String movieName2 = "300";

        Movie movie1 = new Movie(movieName1);
        Movie movie2 = new Movie(movieName2);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(objectMapper.writeValueAsString(movie2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));


        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value(movieName1))
                .andExpect(jsonPath("$.[1].id").value(1))
                .andExpect(jsonPath("$.[1].name").value(movieName2));
    }

    //Testfall 5
    @Test
    void createMovieWithWrongJSONKeyNameAndCheckForExistence() throws Exception {
        String movieJson = "{\"namos\": \"Die Unglaublichen\"}";

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/movies")
                        .content(movieJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").doesNotExist());
    }


    //Testfall 6
    @Test
    void createTwoMoviesWithOneInvalidInputAndCheckForAllMovies() throws Exception {

        String movie = "{\"namos\": \"Die Unglaublichen\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(movie)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        String movie1 = "{\"name\": \"San Andreas\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/movies")
                .content(movie1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/movies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(0))
                .andExpect(jsonPath("$.[0].name").value("San Andreas"))
                .andExpect(jsonPath("$.*", hasSize(1)));

    }
}