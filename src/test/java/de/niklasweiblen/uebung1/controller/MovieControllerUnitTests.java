package de.niklasweiblen.uebung1.controller;

import de.niklasweiblen.uebung1.entity.Movie;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class MovieControllerUnitTests {

    MovieController movieController;

    @BeforeEach
    void setUp() {
        this.movieController = new MovieController();
    }

    @Test
    void testGetMovieMethod() {
        ResponseEntity<List<Movie>> allMovies = movieController.getAllMovies();
        Assertions.assertThat(allMovies.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

    @Test
    void testAddMovieMethodWithEmptyMovieName() {
        Movie movie = new Movie("");
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        Assertions.assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    void testAddMovieMethodWithoutMovieName() {
        Movie movie = new Movie();
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        Assertions.assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    void testAddMovieMethodWithMovieNameAndId() {
        String movieName = "Kingsman: The Secret Service";
        Movie movie = new Movie(movieName);
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);
        Assertions.assertThat(movieResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void checkForAddedMovie() {

        String movieName = "Kingsman: The Secret Service";
        Movie movie = new Movie(movieName);
        ResponseEntity<Movie> movieResponseEntity = movieController.addMovie(movie);

        Movie expectedMovie = new Movie(0L, movieName);

        ResponseEntity<List<Movie>> allMovies = movieController.getAllMovies();
        List<Movie> allMoviesList = allMovies.getBody();

        Assertions.assertThat(allMovies.getStatusCode()).isEqualTo(HttpStatus.OK);
        assert allMoviesList != null;
        Assertions.assertThat(allMoviesList.get(0).getId()).isEqualTo(expectedMovie.getId());


    }
}
